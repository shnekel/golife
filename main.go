package main

import (
	"fmt"
	"github.com/gdamore/tcell/v2"
	"github.com/mattn/go-runewidth"
	"log"
	"math/rand"
	"time"
)

func main() {
	s, err := tcell.NewScreen()
	if err != nil {
		log.Fatal(err)
	}
	if err := s.Init(); err != nil {
		log.Fatal(err)
	}

	defStyle := tcell.StyleDefault.Background(tcell.ColorReset).Foreground(tcell.ColorReset)
	chStyle := tcell.StyleDefault.Foreground(tcell.ColorWhite).Background(tcell.ColorReset)
	cellStyle := tcell.StyleDefault.Foreground(tcell.ColorWhite).Background(tcell.ColorWhite)

	rand.Seed(time.Now().UnixNano())

	var gen int
	var speed int = 50
	width, height := s.Size()

	SizeX := width
	SizeY := height - 5

	board := boardInit(SizeX, SizeY)
	newBoard := boardInit(SizeX, SizeY)

	boardReset(&board, SizeX, SizeY)

	s.SetStyle(defStyle)
	s.EnableMouse()
	s.EnablePaste()
	s.Clear()

	quit := make(chan struct{})
	go func() {
		for {
			ev := s.PollEvent()
			switch ev := ev.(type) {
			case *tcell.EventKey:
				if ev.Key() == tcell.KeyEscape {
					close(quit)
					return
				} else if ev.Key() == tcell.KeyEnter {
					board[rand.Intn(SizeX)][rand.Intn(SizeY)] = true
				} else if ev.Rune() == 'r' {
					boardReset(&board, SizeX, SizeY)
					gen = 0
				} else if ev.Rune() == '-' {
					if speed-5 > 0 {
						speed -= 5
					}
				} else if ev.Rune() == '=' {
					speed += 5
				}
			case *tcell.EventResize:
				s.Sync()
			case *tcell.EventMouse:
				x, y := ev.Position()
				switch ev.Buttons() {
				case tcell.Button1:
					if x < SizeX && y < SizeY {
						board[x][y] = true
					}
				default:
					emitStr(s, 1, height-3, chStyle, fmt.Sprintf("Mouse X %d Y %d", x, y))
				}
			}

		}
	}()
loop:
	for {
		select {
		case <-quit:
			break loop
		case <-time.After(time.Millisecond * time.Duration(speed)):
		}

		s.Show()

		for i := 0; i < SizeX; i++ {
			for j := 0; j < SizeY; j++ {
				if board[i][j] {
					s.SetCell(i, j, cellStyle, '@')
				} else {
					s.SetCell(i, j, defStyle, '.')
				}
			}
		}

		emitStr(s, 1, height-2, chStyle, fmt.Sprintf("Generations: %d", gen))
		emitStr(s, 1, height-1, chStyle, fmt.Sprintf("Speed: %d", speed))
		gen++

		for i := 0; i < SizeX; i++ {
			for j := 0; j < SizeY; j++ {
				newBoard[i][j] = isAlive(board, i, j, SizeX, SizeY)
			}
		}
		board = newBoard
		newBoard = boardInit(SizeX, SizeY)
	}

	s.Fini()
	fmt.Print(s.Size())
}

func rand1() bool {
	return rand.Float32() < 0.3
}

func boardReset(board *[][]bool, sizex, sizey int) {
	for i := 0; i < sizex; i++ {
		for j := 0; j < sizey; j++ {
			(*board)[i][j] = rand1()
		}
	}
}

func boardInit(sizex, sizey int) [][]bool {
	board := make([][]bool, sizex)
	for i := range board {
		board[i] = make([]bool, sizey)
	}
	return board
}

func emitStr(s tcell.Screen, x, y int, style tcell.Style, str string) {
	for _, c := range str {
		var comb []rune
		w := runewidth.RuneWidth(c)
		if w == 0 {
			comb = []rune{c}
			c = ' '
			w = 1
		}
		s.SetContent(x, y, c, comb, style)
		x += w
	}
}

// ANSI ESC sequence to clear screen. Might not work with Win
//fmt.Print("\033[H\033[2J")

func isAlive(board [][]bool, x, y, sizex, sizey int) bool {
	var live_neigbours int
	start_range_x := 1
	start_range_y := 1
	end_range_x := 1
	end_range_y := 1

	if x == 0 {
		start_range_x = 0
	}
	if x == sizex-1 {
		end_range_x = 0
	}
	if y == 0 {
		start_range_y = 0
	}
	if y == sizey-1 {
		end_range_y = 0
	}

	for i := x - start_range_x; i <= x+end_range_x; i++ {
		for j := y - start_range_y; j <= y+end_range_y; j++ {
			if board[i][j] {
				live_neigbours += 1
			}
		}
	}
	if board[x][y] {
		live_neigbours -= 1
	}
	// if cell alive and has 2 or 3 neighbours
	if board[x][y] && (live_neigbours == 2 || live_neigbours == 3) {
		return true
	}
	// if cell is not alive and has 3 neighbours
	if !board[x][y] && live_neigbours == 3 {
		return true
	}

	return false
}
