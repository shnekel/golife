module gitlab.com/shnekel/golife

go 1.16

require (
	github.com/gdamore/tcell/v2 v2.2.0
	github.com/mattn/go-runewidth v0.0.10
)
